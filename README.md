# Ultraminimal

Minimalistic single file webpage that you can use as a blog. Inspired by [motherfuckingwebsite.com](http://motherfuckingwebsite.com/).

## Features

* Everything in a single file
* CSS-only routing between articles
* List mode (display all articles headers)
* Only 10 CSS rules
* No Javascript

## Caveats

* Avoid ```<img>``` tags since they will [all be downloaded at page load](http://www.quirksmode.org/css/displayimg.html), or use Javascript to dynamically set the ```src``` attribute when appropriate.